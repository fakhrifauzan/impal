<?php
	class Matkul_model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function getDataMatkul() {
		$this->db->select('*');
		$this->db->from('matkul');
		$data = $this->db->get();
		return $data->result();
	}
}
?>
