<?php
	class Kelas_model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function getDataKelas() {
		$this->db->select('*');
		$this->db->from('kelas');
		$data = $this->db->get();
		return $data->result();
	}
}
?>
