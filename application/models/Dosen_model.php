<?php
	class Dosen_model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function getDataDosen() {
		$this->db->select('*');
		$this->db->from('dosen');
		$data = $this->db->get();
		return $data->result();
	}
}
?>
