<?php
	class Mahasiswa_model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function getDataMahasiswa() {
		$this->db->select('*');
		$this->db->from('mahasiswa');
		$data = $this->db->get();
		return $data->result();
	}
}
?>
