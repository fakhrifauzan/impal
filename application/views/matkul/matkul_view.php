<div class="container-fluid">

  <!-- Example Tables Card -->
  <div class="card mb-3">
    <div class="card-header">
      <i class="fa fa-table"></i>
      Data Mata Kuliah
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" width="100%" id="dataTable" cellspacing="0">
          <thead>
            <tr>
              <th>Kode Kelas</th>
              <th>Nama Matakuliah</th>
              <th>SKS</th>
              <th>Fakultas</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach($matkul as $value){
              echo "
              <tr>
                <td>$value->kode_matkul</td>
                <td>$value->nama_matkul</td>
                <td>$value->sks</td>
                <td>$value->fakultas</td>
              </tr>";
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
