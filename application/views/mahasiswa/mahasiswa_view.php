<div class="container-fluid">

  <!-- Example Tables Card -->
  <div class="card mb-3">
    <div class="card-header">
      <i class="fa fa-table"></i>
      Data Mahasiswa
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" width="100%" id="dataTable" cellspacing="0">
          <thead>
            <tr>
              <th>NIM</th>
              <th>Nama</th>
              <th>Fakultas</th>
              <th>Program Studi</th>
              <th>Kelas</th>
              <th>Tahun Masuk</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach($mahasiswa as $value){
              echo "
              <tr>
                <td>$value->nim</td>
                <td>$value->nama</td>
                <td>$value->fakultas</td>
                <td>$value->prodi</td>
                <td>$value->kelas</td>
                <td>$value->tahun_masuk</td>
              </tr>";
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
