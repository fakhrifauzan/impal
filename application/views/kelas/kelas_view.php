<div class="container-fluid">

  <!-- Example Tables Card -->
  <div class="card mb-3">
    <div class="card-header">
      <i class="fa fa-table"></i>
      Data Kelas
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" width="100%" id="dataTable" cellspacing="0">
          <thead>
            <tr>
              <th>Kode Kelas</th>
              <th>Fakultas</th>
              <th>Program Studi</th>
              <th>Dosen Wali</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach($kelas as $value){
              echo "
              <tr>
                <td>$value->kode_kelas</td>
                <td>$value->fakultas</td>
                <td>$value->prodi</td>
                <td>$value->doswal</td>
              </tr>";
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
