<div class="container-fluid">

  <!-- Example Tables Card -->
  <div class="card mb-3">
    <div class="card-header">
      <i class="fa fa-table"></i>
      Data Dosen
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" width="100%" id="dataTable" cellspacing="0">
          <thead>
            <tr>
              <th>Kode Dosen</th>
              <th>Nama</th>
              <th>Fakultas</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach($dosen as $value){
              echo "
              <tr>
                <td>$value->kode_dosen</td>
                <td>$value->nama_dosen</td>
                <td>$value->fakultas</td>
                <td>$value->status</td>
              </tr>";
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
