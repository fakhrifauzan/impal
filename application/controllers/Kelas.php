<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {

	public function __construct() {
    parent::__construct();
    $this->load->model('Kelas_model');
  }

	public function index()	{
		$data['kelas'] = $this->Kelas_model->getDataKelas();
		$this->template->load('template','Kelas/kelas_view',$data);
	}

	public function test(){
		$this->load->library('unit_test');
		$data = $this->Kelas_model->getDataKelas();
		$test_name = 'Function getDataKelas()';
    $this->unit->run($data, 'is_array', $test_name);
    echo $this->unit->report();
	}
}
