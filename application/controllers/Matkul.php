<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Matkul extends CI_Controller {

	public function __construct() {
    parent::__construct();
    $this->load->model('Matkul_model');
  }

	public function index()	{
		$data['matkul'] = $this->Matkul_model->getDataMatkul();
		$this->template->load('template','matkul/matkul_view',$data);
	}

	public function test(){
		$this->load->library('unit_test');
		$data = $this->Matkul_model->getDataMatkul();
		$test_name = 'Function getDataMatkul()';
    $this->unit->run($data, 'is_array', $test_name);
    echo $this->unit->report();
	}
}
