<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {

	public function __construct() {
    parent::__construct();
    $this->load->model('Dosen_model');
  }

	public function index()	{
		$data['dosen'] = $this->Dosen_model->getDataDosen();
		$this->template->load('template','dosen/dosen_view',$data);
	}

	public function test(){
		$this->load->library('unit_test');
		$data = $this->Dosen_model->getDataDosen();
		$test_name = 'Function getDataDosen()';
    $this->unit->run($data, 'is_array', $test_name);
    echo $this->unit->report();
	}
}
