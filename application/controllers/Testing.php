<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testing extends CI_Controller {

	public function __construct() {
    parent::__construct();
		$this->load->library('unit_test');
  }

	public function tambah($a, $b){
		return $a+$b;
	}

	public function index()	{
    $expected_result = 4;
    $test_name = 'Fungsi Penjumlahan';
    $this->unit->run($this->tambah(2,2), $expected_result, $test_name);
    echo $this->unit->report();
	}
}
