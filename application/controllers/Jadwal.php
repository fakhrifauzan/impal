<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends CI_Controller {

	public function __construct() {
    parent::__construct();
    $this->load->model('Jadwal_model');
  }

	public function index()	{
		$data['jadwal'] = $this->Jadwal_model->getDataJadwal();
		$this->template->load('template','jadwal/jadwal_view',$data);
	}

	public function test(){
		$this->load->library('unit_test');
		$data = $this->Jadwal_model->getDataJadwal();
		$test_name = 'Function getDataJadwal()';
    $this->unit->run($data, 'is_array', $test_name);
    echo $this->unit->report();
	}
}
