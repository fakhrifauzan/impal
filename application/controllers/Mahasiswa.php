<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	public function __construct() {
    parent::__construct();
    $this->load->model('Mahasiswa_model');
  }

	public function index()	{
		$data['mahasiswa'] = $this->Mahasiswa_model->getDataMahasiswa();
		$this->template->load('template','mahasiswa/mahasiswa_view',$data);
	}

	public function test(){
		$this->load->library('unit_test');
		$data = $this->Mahasiswa_model->getDataMahasiswa();
		$test_name = 'Function getDataMahasiswa()';
    $this->unit->run($data, 'is_array', $test_name);
    echo $this->unit->report();
	}
}
